# Note:
# It is obviously better to adapt the official Find Doxygen so that 
# whatever variable starting with 'DS_DOXYGEN_' will be auto-inserted into the doxygen configuration file.
# This also lets us remove the auxiliary Doxyfile.in.
# Finally, paths with white spaces are properly escaped in the resulting Doxygen file.

get_property(var_ds_readme_list GLOBAL PROPERTY ds_readme_list)
if (NOT "${var_ds_readme_list}")
    set_property(GLOBAL PROPERTY ds_readme_list)
endif()

# All given paths are converted to absolute paths.
# A relative path is assumed to start from the caller's
# `CMAKE_CURRENT_SOURCE_DIR`.
function(add_readme)
    get_property(tmp GLOBAL PROPERTY ds_readme_list)
    foreach(arg ${ARGV})

        # Transform the path to an absolute path
        # if it is not already.
        if(NOT IS_ABSOLUTE "${arg}")
            set(arg "${CMAKE_CURRENT_SOURCE_DIR}/${arg}")
        endif(NOT IS_ABSOLUTE "${arg}")
        # Add the new file
        set(tmp "${tmp}" "${arg}")
    endforeach()

    set_property(GLOBAL PROPERTY ds_readme_list "${tmp}")
endfunction(add_readme)

find_package(Doxygen
    REQUIRED # dot
    )
    
# This is needed for referring to this directory from a CMake function
set(DS_DOXYGEN_LIST_DIR "${CMAKE_CURRENT_LIST_DIR}")

set(DS_DOXYGEN_IN "${DS_DOXYGEN_LIST_DIR}/Doxyfile.in")
set(DS_DOXYGEN_OUT "${CMAKE_CURRENT_BINARY_DIR}/Doxyfile")

set(DS_DOXYGEN_EXCLUDE_PATTERNS
    "*/.git/* */.svn/* */.hg/* */CMakeFiles/* */*build*/* */_CPack_Packages/* CMakeLists.txt CMakeCache.txt ${DS_DOXYGEN_EXCLUDE_PATTERNS}"
    )

# Output directory
set(DS_DOXYGEN_OUTPUT_DIRECTORY
    "${CMAKE_CURRENT_BINARY_DIR}/doc"
    )

# Merge markdown files in the project.
set(DS_MERGED_MARKDOWN_FILE
    "${CMAKE_CURRENT_BINARY_DIR}/README.md"
    )

set(DS_DOXYGEN_INPUT "${DS_MERGED_MARKDOWN_FILE} ${DS_DOXYGEN_INPUT}")

set(DS_DOXYGEN_INPUT "${CMAKE_CURRENT_SOURCE_DIR} ${DS_DOXYGEN_INPUT}")

# Path from the root directory is allowed for image search in order to allow ReadMe.md
# file with an image
set(DS_DOXYGEN_IMAGE_PATH "${CMAKE_CURRENT_SOURCE_DIR} ${DS_DOXYGEN_IMAGE_PATH}")

set(DS_DOXYGEN_RECURSIVE
    YES
    )

set(DS_DOXYGEN_EXTRACT_ALL YES)

function(add_doxygen_docs
        target_name
        )
        
    # message(FATAL_ERROR ${DS_README_SOURCES})
    get_property(tmp_ds_readme_list GLOBAL PROPERTY ds_readme_list)

    add_custom_target(${target_name}__merge_readme
        COMMAND python3 "${DS_DOXYGEN_LIST_DIR}/cat.py" "${tmp_ds_readme_list}" > "${DS_MERGED_MARKDOWN_FILE}"
        BYPRODUCTS "${DS_MERGED_MARKDOWN_FILE}"
        WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
        COMMENT "Merge the README files specified with the DS_README_SOURCES variable"
        VERBATIM
        )

    # Set the concatenated Markdown file as the main page
    set(DS_DOXYGEN_USE_MDFILE_AS_MAINPAGE
        "${DS_MERGED_MARKDOWN_FILE}"
        )

    # Generate the Doxyfile
    configure_file("${DS_DOXYGEN_IN}" "${DS_DOXYGEN_OUT}" @ONLY)

    add_custom_target("${target_name}"
        ALL # add to global target (all)
        COMMAND "${DOXYGEN_EXECUTABLE}" "${DS_DOXYGEN_OUT}" > /dev/null
        WORKING_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
        DEPENDS ${target_name}__merge_readme
        COMMENT "Generate API documentation with Doxygen"
        VERBATIM
        )

endfunction()

